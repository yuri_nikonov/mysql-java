import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

class MysqlConnection {
    private static Statement stmt;
    private static ResultSet rs;
    private static Connection conn;

    public static void main(String[] args) throws Exception {

        try {
            String url = "jdbc:mysql://localhost/test";
            String username = "java";
            String password = "password";
            String query = "select * from mytable order by id desc";
            conn = DriverManager.getConnection(url, username, password);
            System.out.println("Connection to DB successfull!");
            stmt = conn.createStatement();
            rs = stmt.executeQuery(query);
            while (rs.next()) {
                int count = rs.getInt(1);
                String name = rs.getString(2);
                System.out.println("Id: " + count + " ; Name: " + name);
            }
        }
        catch(Exception ex) {
            System.out.println("Connection failed...");
            System.out.println(ex);
        }
        finally {
            try {
                conn.close();
            }
            catch(SQLException se) {}
            try {
                stmt.close();
            }
            catch(SQLException se) {}
            try {
                rs.close();
            }
            catch(SQLException se) {}
                
        }
    }

}