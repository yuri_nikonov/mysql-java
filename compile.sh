#!/usr/bin/env bash

jdbc_url="https://dev.mysql.com/get/Downloads/Connector-J"
jdbc_url_file="mysql-connector-java-8.0.23.zip"
jdbc_file="mysql-connector-java-8.0.23.jar"
download_command="wget ${jdbc_url}/${jdbc_url_file}"

if [ ! -f $jdbc_file ]; then
    if [ ! -f $jdbc_url_file ]; then
        $download_command
    fi
    unzip -j ${jdbc_url_file} mysql-connector-java-8.0.23/${jdbc_file}
fi

javac MysqlConnection.java
